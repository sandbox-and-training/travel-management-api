from django.db import models

class Account(models.Model):
    account_name = models.CharField(max_length=100)

    def __str__(self):
        return self.account_name


class TravelRequest(models.Model):
    request_name = models.CharField(max_length=100)
    start_date = models.DateField()
    end_date = models.DateField()
    duration = models.IntegerField()
    purpose = models.CharField(max_length=200)
    employee = models.CharField(max_length=100)
    total_expenses = models.DecimalField(max_digits=10, decimal_places=2)
    account = models.ForeignKey(Account, related_name='travel_requests', on_delete=models.CASCADE)

    def __str__(self):
        return self.request_name


class ExpenseItem(models.Model):
    name = models.CharField(max_length=100)
    expense_type = models.CharField(max_length=100)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    date_exp = models.DateField()
    request = models.ForeignKey(TravelRequest, related_name='expense_items', on_delete=models.CASCADE)

    def __str__(self):
        return self.name
