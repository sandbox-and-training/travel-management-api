from rest_framework import serializers
from .models import ExpenseItem, Account, TravelRequest

class ExpenseItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExpenseItem
        fields = '__all__'


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = '__all__'


class TravelRequestSerializer(serializers.ModelSerializer):
    expense_items = ExpenseItemSerializer(many=True, read_only=True)

    class Meta:
        model = TravelRequest
        fields = '__all__'
