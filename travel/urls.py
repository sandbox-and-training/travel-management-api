from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import ExpenseItemViewSet, AccountViewSet, TravelRequestViewSet

router = DefaultRouter()
router.register(r'expenseItems', ExpenseItemViewSet)
router.register(r'accounts', AccountViewSet)
router.register(r'travelRequests', TravelRequestViewSet)

urlpatterns = [
    path('', include(router.urls)),
]