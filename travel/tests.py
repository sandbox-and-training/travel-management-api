from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from .models import Account, TravelRequest, ExpenseItem
from rest_framework_simplejwt.tokens import RefreshToken

class AccountTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='testpass')
        self.token = RefreshToken.for_user(self.user).access_token
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')

    def test_create_account(self):
        url = reverse('account-list')
        data = {'account_name': 'Corporate Account'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_accounts(self):
        url = reverse('account-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TravelRequestTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='testpass')
        self.token = RefreshToken.for_user(self.user).access_token
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        self.account = Account.objects.create(account_name='Corporate Account')

    def test_create_travel_request(self):
        url = reverse('travelrequest-list')
        data = {
            'request_name': 'Business Trip to Berlin',
            'start_date': '2023-09-01',
            'end_date': '2023-09-05',
            'duration': 4,
            'purpose': 'Client Meeting',
            'employee': 'Juan Gomez',
            'total_expenses': 1500.0,
            'account': self.account.id
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_travel_requests(self):
        url = reverse('travelrequest-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class ExpenseItemTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='testpass')
        self.token = RefreshToken.for_user(self.user).access_token
        self.client.credentials(HTTP_AUTHORIZATION=f'Bearer {self.token}')
        self.account = Account.objects.create(account_name='Corporate Account')
        self.travel_request = TravelRequest.objects.create(
            request_name='Business Trip to Berlin',
            start_date='2023-09-01',
            end_date='2023-09-05',
            duration=4,
            purpose='Client Meeting',
            employee='Juan Gomez',
            total_expenses=1500.0,
            account=self.account
        )

    def test_create_expense_item(self):
        url = reverse('expenseitem-list')
        data = {
            'name': 'Hotel Stay',
            'expense_type': 'Accommodation',
            'amount': 200.0,
            'date_exp': '2023-12-20',
            'request': self.travel_request.id
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_expense_items(self):
        url = reverse('expenseitem-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)