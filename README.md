# Travel Management API

This API was created as a task for the course Enterprise Information Technology Architecture, Applications, and Integration taken at Riga Technical University (RTU).

## Overview

The Travel Management API is designed to manage travel requests and expenses efficiently. It allows users to create, retrieve, update, and delete travel requests, expense items, and accounts associated with these requests.

## Features

- JWT Authentication
- CRUD operations for:
  - Expense Items
  - Accounts
  - Travel Requests

## Installation

1. **Clone the repository:**

    ```sh
    git clone https://gitlab.com/sandbox-and-training/travel-management-api.git
    cd travel-management-api
    ```

2. **Create a virtual environment and activate it:**

    ```sh
    python -m venv venv
    source venv/bin/activate  # On Windows use `venv\Scripts\activate`
    ```

3. **Install the required packages:**

    ```sh
    pip install -r requirements.txt
    ```

4. **Apply migrations:**

    ```sh
    python manage.py makemigrations
    python manage.py migrate
    ```

5. **Create a superuser:**

    ```sh
    python manage.py createsuperuser
    ```

6. **Run the development server:**

    ```sh
    python manage.py runserver
    ```

## API Endpoints

### Authentication

- Obtain Token: `POST /api/v1/token/`
- Refresh Token: `POST /api/v1/token/refresh/`

### Expense Items

- List Expense Items: `GET /api/v1/expenseItems/`
- Create Expense Item: `POST /api/v1/expenseItems/`
- Retrieve Expense Item: `GET /api/v1/expenseItems/{id}/`
- Update Expense Item: `PUT /api/v1/expenseItems/{id}/`
- Delete Expense Item: `DELETE /api/v1/expenseItems/{id}/`

### Accounts

- List Accounts: `GET /api/v1/accounts/`
- Create Account: `POST /api/v1/accounts/`
- Retrieve Account: `GET /api/v1/accounts/{id}/`
- Update Account: `PUT /api/v1/accounts/{id}/`
- Delete Account: `DELETE /api/v1/accounts/{id}/`

### Travel Requests

- List Travel Requests: `GET /api/v1/travelRequests/`
- Create Travel Request: `POST /api/v1/travelRequests/`
- Retrieve Travel Request: `GET /api/v1/travelRequests/{id}/`
- Update Travel Request: `PUT /api/v1/travelRequests/{id}/`
- Delete Travel Request: `DELETE /api/v1/travelRequests/{id}/`

## Running Tests

To run the tests, use the following command:

```sh
python manage.py test
